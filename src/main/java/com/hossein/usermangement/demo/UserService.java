package com.hossein.usermangement.demo;

import model.User;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
@Component("UserService")
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class UserService {
    ArrayList<User>registerUsers=new ArrayList<>();

    public ArrayList<User> addUser(Integer id, String name, String family, String password, Integer age, String email, String telephone, String role) {
        User user = new User();
        user.setId(id);
        user.setName(name);
        user.setFamily(family);
        user.setPassword(password);
        user.setAge(age);
        user.setEmail(email);
        user.setTelephone(telephone);
        user.setRole(role);

        registerUsers.add(user);

        return registerUsers;
    }
    public String getAll(){
        for (User u:registerUsers) {
          return u.getId()+u.getName()+u.getFamily()+u.getAge()+u.getEmail()+u.getRole()+u.getTelephone();

        }
        return null;
    }
}
