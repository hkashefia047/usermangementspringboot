package com.hossein.usermangement.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/register")
    public String register(@RequestParam Integer id, @RequestParam String name, @RequestParam String family, @RequestParam String password, @RequestParam Integer age, @RequestParam String email, @RequestParam String telephone, @RequestParam String role) {

        userService.addUser(id, name, family, password, age, email, telephone, role);
        return "id: " + id + " name: " + name + " family: " + family + " age: " + age + " email: " + email + "phone: " + telephone + " role: " + role;
    }

    @RequestMapping("/allInformation")
    public String getAll() {
        return userService.getAll();
    }


}
