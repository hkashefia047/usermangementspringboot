package model;

public class User extends Person {
    private Integer id;
    private String password;
    private String Role;

    public User() {
    }

    public User(Integer id, String name, String family, String password, Integer age, String email, String telephone, String role) {
        super(name, family, age, email, telephone);
        this.id = id;
        this.password = password;
        Role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }
}
