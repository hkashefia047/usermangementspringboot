package model;

public abstract class Person {

    private String name;
    private String family;
    private Integer age;
    private String email;
    private String telephone;

    public Person() {
    }

    public Person(String name, String family, Integer age, String email, String telephone) {
        this.name = name;
        this.family = family;
        this.age = age;
        this.email = email;
        this.telephone = telephone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
